export class Client {
  id: string;
  name: string;
  archivedAt: string;
  createdAt: string;
  lastModifiedAt: string;
  accountId: string;
}
