import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FetchDataService } from './fetch-data.service';

import { Project } from './project/project';

import { AppComponent } from './app.component';
import { AccountComponent } from './account/account.component';
import { ClientComponent } from './client/client.component';
import { ClientListComponent } from './client-list/client-list.component';
import { EmployeeComponent } from './employee/employee.component';
import { ProjectComponent } from './project/project.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { AppRoutingModule } from './/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    ClientComponent,
    ClientListComponent,
    EmployeeComponent,
    ProjectComponent,
    ProjectListComponent,
    SidebarComponent,
    TaskListComponent,
    TimesheetComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [FetchDataService, Project],
  bootstrap: [AppComponent]
})
export class AppModule { }
