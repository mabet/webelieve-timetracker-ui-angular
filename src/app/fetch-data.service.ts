import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Account } from './account/account';
import { Project } from './project/project';
import { Client } from './client/client';

@Injectable()
export class FetchDataService {

  constructor(private _http: HttpClient) { }

  currentAccount: string = localStorage.getItem("currentAccount");
  changeAccount(accountId: string) {
    this.currentAccount = accountId;
  }

  removeItem(list, data) {
    list.splice(list.indexOf(data), 1);
  }

  getAccounts() {
    return this._http.get<Account[]>('http://localhost:5000/accounts');
  }

  //----------------------------------------------------------------------------------------------------Clients
  getClients() {
    return this._http.get<Client[]>('http://localhost:5000/clients?accountId=' + this.currentAccount);
  }
  postClient(data) {
    return this._http.post('http://localhost:5000/clients', data);
  }
  deleteClient(data) {
    return this._http.delete('http://localhost:5000/clients/' + data);
  }
  archiveClient(data) {
    return this._http.patch('http://localhost:5000/clients/' + data + '/archive'); //why the little red line...? this request only takes one argument...
  }

  //----------------------------------------------------------------------------------------------------Employees
  getEmployees() {
    return this._http.get('http://localhost:5000/employees?accountId=' + this.currentAccount);
  }

  //----------------------------------------------------------------------------------------------------Projects
  getProjects() {
    return this._http.get<Project[]>('http://localhost:5000/projects?accountId=' + this.currentAccount);
  }
  postProject(data) {
    return this._http.post('http://localhost:5000/projects', data);
  }
  deleteProject(projectId) {
    return this._http.delete('http://localhost:5000/projects/' + projectId);
  }
  archiveProject(projectId) {
    return this._http.patch('http://localhost:5000/projects/' + projectId + '/archive');
  }
  getProject(projectId) {
    return this._http.get<Project>('http://localhost:5000/projects/' + projectId);
  }

  //----------------------------------------------------------------------------------------------------Tasks
  getTasks() {
    return this._http.get('http://localhost:5000/tasks?accountId=' + this.currentAccount);
  }

  //----------------------------------------------------------------------------------------------------Timesheet
  getTimesheet() {
    return this._http.get('http://localhost:5000/timesheet?accountId=' + this.currentAccount);
  }
}
