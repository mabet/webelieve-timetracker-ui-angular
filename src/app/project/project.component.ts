import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FetchDataService } from '../fetch-data.service';
import { Observable } from 'rxjs/Observable';
import { Chart } from 'chart.js';
import { Project } from '../project/project';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  
  constructor(private _fetchDataService: FetchDataService, private location: Location) { }

  projectId: string = this.location.path().slice(9);
  project = new Project();
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  monthsToDisplay = ['January', 'February', 'March', 'April', 'May', 'June'];
  years = [];
  thisYear = new Date().getFullYear();
  thisMonthI = new Date().getMonth();
  startYear = 0;
  startMonth = '';
  combDates = [];
  combDatesToDisplay = [];
  chart = [];  

  ngOnInit() {
    this.getProject();
    this.currentDate();
  }

  getProject() {
    this._fetchDataService.getProject(this.projectId).subscribe(
      data => { this.project = data },
      err => console.error(err)
    );
  }

  selectedMonth: string;
  selectedYear: string;
  hideMonths: boolean = true;
  hideYears: boolean = true;
  hideCombDate: boolean = true;

  currentDate() {
    this.selectedMonth = this.months[new Date().getMonth()];
    this.selectedYear = new Date().getFullYear().toString();
  }

  getYears(data) {
    this.startYear = parseInt(data.substr(0, 4), 10);
    this.years = [];
    if (this.thisYear > this.startYear) {
      for (let i = this.thisYear; i >= this.startYear; i--) {
        this.years.push(i.toString());
      }
    }
  }

  getCombDates(data) {
    this.combDates = [];
    this.combDatesToDisplay = [];
    this.startYear = parseInt(data.substr(0, 4), 10);
    let startMonthI = parseInt(data.substr(5, 7), 10) - 6;
    this.startMonth = this.months[startMonthI];
    if (this.thisYear > this.startYear) {
      for (let i = this.thisYear; i >= this.startYear; i--)
        if (i > this.startYear) {
          for (let x = this.thisMonthI; x >= 0; x--) {
            this.combDates.push({ month: this.months[x], year: i })
          }
        } else if (i == this.startYear) {
          for (let x = 11; x >= startMonthI; x--) {
            this.combDates.push({ month: this.months[x], year: i })
          }
        }
    } else if (this.thisYear == this.startYear && this.thisMonthI > startMonthI) {
      for (let x = this.thisMonthI; x >= startMonthI; x--) {
        this.combDates.push({ month: this.months[x], year: this.thisYear })
      }
    }
    for (let y = 0; y <= 2; y++) {
      this.combDatesToDisplay.push(this.combDates[y])
    }
  }

  scrollMonths(direction) {
    let x = this.months.indexOf(this.monthsToDisplay[0]);
    let y = x + 6;
    if (direction == 0) {
      if (x > 0) {
        x -= 1;
        y -= 1;
        this.monthsToDisplay = this.months.slice(x, y);
      }
    } else if (direction == 1) {
      if (x < 6) {
        x += 1;
        y += 1;
        this.monthsToDisplay = this.months.slice(x, y);
      }
    }
  }

  scrollDates(direction) {
    let x = this.combDates.indexOf(this.combDatesToDisplay[0]);
    let y = x + 3;
    if (direction == 0) {
      if (x > 0) {
        x -= 1;
        y -= 1;
        this.combDatesToDisplay = this.combDates.slice(x, y);
      }
    } else if (direction == 1) {
      if (y < this.combDates.length) {
        x += 1;
        y += 1;
        this.combDatesToDisplay = this.combDates.slice(x, y);
      }
    }
  }

  setMonth(month) {
    this.selectedMonth = month;
    this.hideMonths = true;
    this.hideCombDate = true;
  }

  setYear(year) {
    this.selectedYear = year;
    this.hideYears = true;
  }

  toggleMonths() {
    if (this.hideMonths == true) {
      this.hideMonths = false;
      this.hideYears = true;
      this.hideCombDate = true;
    } else {
      this.hideMonths = true;
    }
  }
  toggleYears() {
    if (this.hideYears == true) {
      this.hideYears = false;
      this.hideMonths = true;
      this.hideCombDate = true;
    } else {
      this.hideYears = true;
    }
  }
  toggleCombDate() {
    if (this.hideCombDate == true) {
      this.hideCombDate = false;
      this.hideMonths = true;
      this.hideYears = true;
    } else {
      this.hideCombDate = true;
    }
  }



  changeSet() {

  }

  //chartLabels = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
  //chartData = [8, 7, 8, 12, 5.5, 2, 0, 10, 15.5, 9, 14, 8, 3, 0, 12, 13, 9, 9.75, 8, 1];
  //barColors = [];
  //barColorClick = [];
  //vertLinesColor = [];

  //buildChart() {
  //  this.chart = new Chart('canvas', {
  //    type: 'bar',
  //    data: {
  //      labels: this.chartLabels,
  //      datasets: [{
  //        label: '',
  //        data: this.chartData,
  //        backgroundColor: this.barColors,
  //        hoverBackgroundColor: this.barColorClick
  //      }]
  //    },
  //    options: {
  //      legend: {
  //        display: false
  //      },
  //      events: ['click'],
  //      scales: {
  //        xAxes: [{
  //          barPercentage: 0.9,
  //          categoryPercentage: 1,
  //          gridLines: {
  //            offsetGridLines: false,
  //            color: this.vertLinesColor,
  //            borderDash: [3, 5],
  //            lineWidth: 2
  //          }
  //        }],
  //        yAxes: [{
  //          gridLines: {
  //            color: 'rgba(168,159,142,0.75)',
  //            borderDash: [3, 2],
  //            lineWidth: 2
  //          },
  //          ticks: {
  //            min: 0,
  //            max: 16,
  //            stepSize: 4
  //          }
  //        }]
  //      }//,
  //      //tooltips: {
  //      //  enabled: false,
  //      //  mode: 'index',
  //      //  position: 'nearest',
  //      //  custom: function (tooltip) {
  //      //    var tooltipEl = document.getElementById('chartjs-tooltip');

  //      //    if (tooltip.opacity === 0) {
  //      //      tooltipEl.style.opacity = 0;
  //      //      return;
  //      //    }
  //      //    // Set caret Position
  //      //    tooltipEl.classList.remove('above', 'below', 'no-transform');
  //      //    if (tooltip.yAlign) {
  //      //      tooltipEl.classList.add(tooltip.yAlign);
  //      //    } else {
  //      //      tooltipEl.classList.add('no-transform');
  //      //    }

  //      //    function getBody(bodyItem) {
  //      //      return bodyItem.lines;
  //      //    }
  //      //    // Set Text
  //      //    if (tooltip.body) {
  //      //      var titleLines = tooltip.title || [];
  //      //      var bodyLines = tooltip.body.map(getBody);

  //      //      var innerHtml = '<tbody>';

  //      //      titleLines.forEach(function (title) {
  //      //        innerHtml += '<tr><td class="tooltip-left">' + title + '</td>';
  //      //      });
  //      //      bodyLines.forEach(function (body, i) {
  //      //        innerHtml += '<td class="tooltip-right">' + body + '<span><i class="chevron right icon" data-bind="click: breakdown"></i></span></td>';
  //      //      });
  //      //      innerHtml += '</tr></tbody>';

  //      //      var tableRoot = tooltipEl.querySelector('table');
  //      //      tableRoot.innerHTML = innerHtml;
  //      //    }

  //      //    var positionX = this._chart.canvas.offsetLeft;
  //      //    var positionY = this._chart.canvas.offsetTop;
  //      //    var offsetX = 0;
  //      //    var offsetY = 0;
  //      //    var tooltipWidth = $('#chartjs-tooltip').width();
  //      //    var tooltipHeight = $('#chartjs-tooltip').height();

  //      //    if (((tooltip.caretX) / chartWidth) <= 0.5) {
  //      //      offsetX = tooltipWidth / 2;
  //      //    } else {
  //      //      offsetX = -(tooltipWidth / 2);
  //      //    }
  //      //    if (tooltipHeight >= (chartHeight - tooltip.caretY)) {
  //      //      offsetY = -(chartHeight - tooltip.caretY);
  //      //    } else if ((tooltip.caretY / chartHeight) >= 0.5) {
  //      //      offsetY = -(tooltipHeight / 2);
  //      //    } else {
  //      //      offsetY = tooltipHeight / 2;
  //      //    }

  //      //    tooltipEl.style.opacity = 1;
  //      //    tooltipEl.style.left = positionX + tooltip.caretX + offsetX + 'px';
  //      //    tooltipEl.style.top = positionY + tooltip.caretY + offsetY + 'px';

  //      //    console.log(positionY + ',' + tooltip.caretY + ',' + offsetY + ' ' + chartHeight);
  //      //  }
  //      //}
  //    }
  //  });
  //}
}
