//import { Injectable } from '@angular/core';
//import { Observable } from 'rxjs/Observable';
//@Injectable()
export class Project {
  archivedAt: string;
  clientId: string;
  clientName: string;
  createdAt: string;
  id: string;
  lastModifiedAt: string;
  name: string;

  //constructor(data) {
  //  this.archivedAt = data.archivedAt;
  //  this.clientId = data.clientId;
  //  this.clientName = data.clientName;
  //  this.createdAt = data.createdAt;
  //  this.id = data.id;
  //  this.lastModifiedAt = data.lastModifiedAt;
  //  this.name = data.name;
  //}
}
