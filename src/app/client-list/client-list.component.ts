import { Component, OnInit } from '@angular/core';
import { FetchDataService } from '../fetch-data.service';
import { Observable } from 'rxjs/Observable';
import { Client } from '../client/client';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {

  constructor(private _fetchDataService: FetchDataService) { }

  clients: Client[];

  ngOnInit() {
    this.getClients();
  }

  newClientName: string = '';

  setName(data) {
    this.newClientName = data.target.value;
  }

  newClientBody = {
    "accountId": this._fetchDataService.currentAccount,
    "name": this.newClientName
  };

  //-----------------------------------------------------------------requests
  getClients() {
    this._fetchDataService.getClients().subscribe(
      data => { this.clients = data },
      err => console.error(err)
    );
  }

  addNewClient() {
    this._fetchDataService.postClient(this.newClientBody).subscribe();
    this.newClientName = '';
  }

  deleteClient(data) {
    this._fetchDataService.deleteClient(data.id).subscribe();
    this._fetchDataService.removeItem(this.clients, data);
    console.log('client deleted');
  }

  archiveClient(data) {
    this._fetchDataService.archiveClient(data.id).subscribe();
    this._fetchDataService.removeItem(this.clients, data);
    console.log('client archived')
  }
  //------------------------------------------------------------------------

  shortDate(date) {
    return date.substr(0, 10);
  }

}
