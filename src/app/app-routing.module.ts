import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountComponent } from './account/account.component';
import { ClientListComponent } from './client-list/client-list.component';
import { EmployeeComponent } from './employee/employee.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TimesheetComponent } from './timesheet/timesheet.component';

import { ProjectComponent } from './project/project.component';
import { ClientComponent } from './client/client.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'account', component: AccountComponent },
  { path: 'client-list', component: ClientListComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: 'project-list', component: ProjectListComponent },
  { path: 'task-list', component: TaskListComponent },
  { path: 'timesheet', component: TimesheetComponent },
  { path: 'client/:id', component: ClientComponent },
  { path: 'project/:id', component: ProjectComponent }
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

