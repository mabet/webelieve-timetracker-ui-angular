import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FetchDataService } from '../fetch-data.service';
import { Observable } from 'rxjs/Observable';
import { Account } from '../account/account';


@Component({
  selector: 'app-sidebar',
  providers: [Location],
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private _fetchDataService: FetchDataService, private router: Router, private location: Location) { }

  accounts: Account[];
  accountId: string;

  ngOnInit() {
    this.getAccounts();
  }

  getAccounts() {
    this._fetchDataService.getAccounts().subscribe(
      data => { this.accounts = data },
      err => console.error(err)
    );
  }

  currentPath: string;

  selectAccount(id) {
    this.accountId = id;
    this._fetchDataService.changeAccount(this.accountId);

    //----------------local storage
    window.localStorage.setItem("currentAccount", this.accountId);
    //-------------------------

    if (this.location.path().includes('/project/') || this.location.path().includes('/client/')) {
      this.toggleList();
      this.router.navigate(['']);
    } else {
      this.currentPath = this.location.path();
      this.toggleList();
      this.router.navigate(['']).then(() =>
        this.router.navigate([this.currentPath]));
    }
  }

  accountsDisplay: boolean = true;
  toggleList() {
    if (this.accountsDisplay == true) {
      this.accountsDisplay = false;
    } else {
      this.accountsDisplay = true;
    }
  }
}
