import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FetchDataService } from '../fetch-data.service';
import { Observable } from 'rxjs/Observable';
import { Project } from '../project/project';
import { Client } from '../client/client';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  constructor(private _fetchDataService: FetchDataService, private router: Router) { }

  projects: Project[];
  clients: Client[];

  ngOnInit() {
    this.getProjects();
    this.getClients();
    console.log(this.projects);
  }

  newProjName: string = '';
  setName(data) {
    this.newProjName = data.target.value;
  }

  newProjClient: string;
  newProjClientName: string = 'select client';
  clientsDisplay: boolean = true;

  setClient(data) {
    this.newProjClient = data.id;
    this.newProjClientName = data.name;
    this.toggleClients();
    this.defaultInput = false;
  }
  
  toggleClients() {
    if (this.clientsDisplay == true) {
      this.clientsDisplay = false;
      this.hasFocus = true;
    } else {
      this.clientsDisplay = true;
      this.hasFocus = false;
    }
  }

  hideClients() {
    this.clientsDisplay = true;
    this.hasFocus = false;
  }

  newProjectBody = {
    "name": this.newProjName,
    "clientId": this.newProjClient
};

  viewProject(id) {
    this.router.navigate(['project/' + id]);
  }

  defaultInput: boolean = true;
  hasFocus: boolean = false;

  //-----------------------------------------------------------------requests
  getProjects() {
    this._fetchDataService.getProjects().subscribe(
      data => { this.projects = data },
      err => console.error(err)
    );
  }

  getClients() {
    this._fetchDataService.getClients().subscribe(
      data => { this.clients = data },
      err => console.error(err)
    );
  }

  addNewProject() {
    this._fetchDataService.postProject(this.newProjectBody).subscribe();
    this.newProjName = '';
    this.newProjClientName = 'select client'
    this.hideClients();
    this.defaultInput = true;
  }

  deleteProject(data) {
    this._fetchDataService.deleteProject(data.id).subscribe();
    this._fetchDataService.removeItem(this.projects, data);
    this.hideClients();
    console.log('project deleted');
  }

  archiveProject(data) {
    this._fetchDataService.archiveProject(data.id).subscribe();
    this._fetchDataService.removeItem(this.projects, data);
    this.hideClients();
    console.log('project archived')
  }
  //------------------------------------------------------------------------

  shortDate(date) {
    return date.substr(0, 10);
  }

}
