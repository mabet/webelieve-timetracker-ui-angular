export class Account {
  id: string;
  name: string;
  archivedAt: string;
  createdAt: string;
  lastModifiedAt: string;
}
