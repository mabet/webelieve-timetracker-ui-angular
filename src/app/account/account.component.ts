import { Component, OnInit } from '@angular/core';
import { FetchDataService } from '../fetch-data.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  constructor(private _fetchDataService: FetchDataService) { }

  accounts: any;

  ngOnInit() {
    this._fetchDataService.getAccounts().subscribe(
      data => { this.accounts = data },
      err => console.error(err)
    );

    console.log(this.accounts);
  }

}
